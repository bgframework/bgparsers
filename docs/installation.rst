.. highlight:: shell

============
Installation
============


Stable release
--------------

To install bgparsers, run this command in your terminal:

.. code-block:: console

    $ pip install bgparsers

This is the preferred method to install bgparsers, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

You can install latest version from the git repository using this command:

.. code-block:: console

    $ pip install git+https://bitbucket.org/bgframework/bgparsers.git


And the sources for bgparsers can be downloaded from the `Bitbucket repo`_.
Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Bitbucket repo: https://bitbucket.org/bgframework/bgparsers/src
