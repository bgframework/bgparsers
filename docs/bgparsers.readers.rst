bgparsers.readers package
=========================

Submodules
----------

bgparsers.readers.common module
-------------------------------

.. automodule:: bgparsers.readers.common
   :members:
   :undoc-members:
   :show-inheritance:

bgparsers.readers.elements module
---------------------------------

.. automodule:: bgparsers.readers.elements
   :members:
   :undoc-members:
   :show-inheritance:

bgparsers.readers.tsv module
----------------------------

.. automodule:: bgparsers.readers.tsv
   :members:
   :undoc-members:
   :show-inheritance:

bgparsers.readers.variants module
---------------------------------

.. automodule:: bgparsers.readers.variants
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: bgparsers.readers
   :members:
   :undoc-members:
   :show-inheritance:
